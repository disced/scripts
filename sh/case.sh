#!/bin/bash
read n
case $n in
    *1* | *5* ) echo Uno o Cinco;;
    *2*) echo Dos;;
    *3*) echo Tres;;
    *4*) echo Cuatro;;
    *) echo Opción incorrecta;;
esac
