#!/bin/bash

## Recibe dos parametros y los imprime en pantalla

if [[ $# -eq 2 ]] # Comprueba si se introducen dos parametros
then
    echo "Parametro numero 1" $1;
    echo "Parametro numero 2" $2;
else
    echo "Introduce dos parametros"
fi
