#!/bin/bash
	###################################
	# Autor: Dragos C. Butnariu
	# Descripción del Script: Opera 2 numeros que se pasa por argumento
        # Si n1 < n2 se suma
        # Si n1 > n2 se resta
        # Si n1 = n2 se multiplica 
	###################################

if [[ $1 =~ ^[+-]?[0-9]+$ ]] &&  [[ $2 =~ ^[+-]?[0-9]+$ ]] # Comprueba si los argumentos 1 y 2 son
                                                           # numeros de 0 al 9
then                                                       # Si son números hace verificaciones para
                                                           # hacer la Suma, la Resta o Multiplicación
  if [[ $1 -lt $2 ]]
  then 
     echo "La suma de $1 más $2 es: " $(( $1+$2 ))
  else
     if [[ $1 -gt $2  ]]
     then 
         echo "La resta de $1 menos $2 es: " $(( $1-$2 ))
     else
          echo "La multiplicación de $1 por $2 es: " $(( $1*$2 ))
      fi
  fi
else
echo "Introduce dos numeros"
fi
