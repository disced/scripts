#!/bin/bash
        ###################################
	# Autor: Dragos C. Butnariu
	# Descripción del Script: Introduces X números en un array y muestra la tabla de
        # multiplicar de cada numero 
	###################################




read -p "Introduce números para visualizar tabla de multiplicar " num
IFS=" " # Cambia el valor de la variable IFS por un espacio para conseguir que funcione el bucle for
for n in $num # Bucle for que recorre todos los numeros introducidos
do
  
     echo "La tabla de multiplicar de $n es :"
  for ((i=0;i<11;i++)) #Bucle for que hace la multiplicación de cada número introducido por los numeros del 0 al 10 
  do
      echo -e "$n * $i =" $(( $n*$i ))
  done
done
echo -e "\nTablas de multiplicar introducidas: ${#num[@]}"

