#!/bin/bash
# Crea un nuevo directorio
# Copia los archivos de un directorio que se introduce y con una extension que tambien se introduce
echo "Nuevo directorio"
read -p "ruta: " nuevod #Variable nuevod para la ruta de entrada
if [ -d $nuevod ]; #Compara si existe el directorio
then
    echo "El directorio ya existe"
else
    echo "Creando directorio"
    mkdir $nuevod
fi

echo "Directorio de donde copiar"
read -p "ruta: " copiad
echo "Extension de los archivos a copiar"
read -p "Extension: " ext
if [ -d $copiad ]
then 
    cp $copiad/*$ext $nuevod
else
    echo "El directorio de donde copiar no existe"
fi
