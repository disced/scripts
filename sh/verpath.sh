#!/bin/bash

#Imprime los directorios de la variable PATH linea por linea

IFS=':' #Es el delimitador que se va a eliminar
lista=$PATH #crea una lista con el valor que hay en $PATH
for path in ${lista}; do
    echo $path
done
