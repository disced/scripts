#!/bin/bash

# Cambia extensión mediante argumentos
# Argumento $1 extension que cambiar por $2


listado=$(ls *.$1) # Variable con la salida del comando entre ()
for archivo in $listado # Bucle for que cambia la extension de cada archivo
do
   mv ${archivo} ${archivo/.$1}.$2
done

