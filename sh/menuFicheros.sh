#!/bin/bash
# Script muestra info de fichero/directorio segun parametro 
# introducido, si es directorio cuenta los archivos, y lo
# lista, si es archivo muestra info sobre el y si no existia
# lo crea



## Funciones
tamañodir () {
    # Funcion que comprueba si es directorio y cuenta el contenido o dice que es archivo
 if [[ -d $1 ]]
  then 
    printf "\n"
    command ls $1 | wc -l
    command echo -e "\rarchivo/s tiene $1"
    printf "\n"
else
    printf "\nEs un archivo con un peso de: "
fi

}
tamaño () {
    #Funcion que muestra el tamaño del archivo/directorio
    command ls -sh $1
    printf "\n"
}
info () {
    #Funcion que muestra info sobre archivo/directorio 
    command ls -lh $1 
    printf "\n"
}
existe () {
    #Si no existe el archivo lo crea e informa de la creación
    if [[ -a $1 ]]
    then
        printf "\n"
    else
        command touch $1
        printf "Fichero $1 creado, no existia\n"
    fi
}

## Lamada a funciones mediante argumento $1

tamañodir $1
tamaño $1
info $1
existe $1
