# Whats Grp

Is a small script that we input backup of a whatsapp conversation and
counts the messages sent by each phone number.

## Input File


```
01/01/2021, 21:09 - +00 000 00 00 01: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:11 - +00 000 00 00 01: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:19 - +00 000 00 00 02: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:21 - +00 000 00 00 02: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:23 - +00 000 00 00 02: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:23 - +00 000 00 00 02: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:31 - +00 000 00 00 03: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:33 - Alice: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:33 - Alice: Lorem ipsum dolor sit amet, consectetur sed do
01/01/2021, 21:37 - Alice: Lorem ipsum dolor sit amet, consectetur sed do
```

## Output

```
+00 000 00 00 02 : 4
Alice : 3
+00 000 00 00 01 : 2
+00 000 00 00 03 : 1
```