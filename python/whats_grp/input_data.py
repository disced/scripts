import os
import message_counter


def ask_file():
    path_file = input('Input absolute path\n')

    if not os.path.isfile(path_file):
        path_file = 'no_file'

    return path_file


def main():
    user_file = ask_file()
    counter = message_counter.Counter(user_file)

    counter.save_data()
    data = counter.return_data()

    print(data)


main()
